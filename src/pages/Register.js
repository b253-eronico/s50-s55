import { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import { Navigate, useNavigate } from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2'

export default function Register(){

	
	const { user } = useContext(UserContext)
	// State hooks to store the values of the input fields
	// getters are variables that store data (from setters)
	// setters are function that sets the data (for the getters)
	const [firstName, setFirstName ] = useState('');
	const [lastName, setLastName ] = useState('');
	const [mobileNumber, setMobileNumber ] = useState('');
	const [email, setEmail] = useState('');
	const [password1, setPassword1] = useState('');
	const [password2, setPassword2] = useState('');
	// State to determine whether submit button is enabled or not
	const [isActive, setIsActive] = useState(false);

	console.log(email);
	console.log(password1);
	console.log(password2);

	// Function to simulate user registration
	async function registerUser(e) {
		// Prevents page redirection via form submission
		e.preventDefault();


		// Check if email exists
		const emailExists = await checkIfEmailExists(email);
		if (emailExists) {
			// Display SweetAlert2 error message
			Swal.fire({
				title: 'Error!',
				text: 'Email already exists. Please use a different email.',
				icon: 'error',
				confirmButtonText: 'OK'
			});
			return;
		}


		// Clear input fields
		setFirstName('');
		setLastName('');
		setMobileNumber('');
		setEmail('');
		setPassword1('');
		setPassword2('');

		Swal.fire({
			title: 'Success!',
			text: 'Thank you for registering!',
			icon: 'success',
			confirmButtonText: 'OK'
		})	

	}

	async function checkIfEmailExists(email) {
			const response = await fetch(`http://localhost:4000/users/check-email?email=${email}`);
			const data = await response.json();
			return data.emailExists;
		}

		const navigate = useNavigate()

	useEffect(() => {
		// Validation to enable submit button when all fields are populated and both passwords match
		if((email !== '' && password1 !== '' && password2 !== '') && (password1 === password2)){
			setIsActive(true);
		} else {
			setIsActive(false);
		}
	}, [email, password1, password2]);

	return(

		(user.id !== null) ?
		    <Navigate to="/courses" />
		:
		// 2-way Binding (Bnding the user input states into their corresponding input fields via the onChange JSX Event Handler)
		<Form onSubmit={(e) => registerUser(e).then(() => navigate('/login'))}> 
		



				<Form.Group className="mb-3" controlId="userFirstName">
				  <Form.Label>First Name</Form.Label>
				  <Form.Control 
				  		type="text" 
				  		placeholder="Enter First Name"
				  		value={ firstName }
				  		onChange={ e => setFirstName(e.target.value) } 
				  		required />
				</Form.Group>

				<Form.Group className="mb-3" controlId="userLastName">
				  <Form.Label>Last Name</Form.Label>
				  <Form.Control 
				  		type="text" 
				  		placeholder="Enter Last Name"
				  		value={ lastName }
				  		onChange={ e => setLastName(e.target.value) } 
				  		required />
				</Form.Group>

				<Form.Group className="mb-3" controlId="userMobileNumber">
				  <Form.Label>Mobile Number</Form.Label>
				  <Form.Control 
				  		type="text" 
				  		placeholder="Enter Mobile Number"
				  		value={ mobileNumber }
				  		onChange={ e => setMobileNumber(e.target.value) } 
				  		required />
				</Form.Group>

		      <Form.Group className="mb-3" controlId="userEmail">
		        <Form.Label>Email address</Form.Label>
		        <Form.Control 
		        		type="email" 
		        		placeholder="Enter email"
		        		value={ email }
		        		onChange={ e => setEmail(e.target.value) } 
		        		required/>
		        <Form.Text className="text-muted">
		          We'll never share your email with anyone else.
		        </Form.Text>
		      </Form.Group>

		      <Form.Group className="mb-3" controlId="password1">
		        <Form.Label>Password</Form.Label>
		        <Form.Control 
		        		type="password" 
		        		placeholder="Password"
		        		value={ password1 }
		        		onChange={ e => setPassword1(e.target.value) }  
		        		required/>
		      </Form.Group>
		      
		      <Form.Group className="mb-3" controlId="password2">
		        <Form.Label>Verify Password</Form.Label>
		        <Form.Control 
		        		type="password" 
		        		placeholder="Verify Password"
		        		value={ password2 }
		        		onChange={ e => setPassword2(e.target.value) }   
		        		required/>
		      </Form.Group>

		      { isActive ? 
		      		<Button variant="primary" type="submit" id="submitBtn">
		        Submit
		      </Button>
		      : <Button variant="danger" type="submit" id="submitBtn" disabled>
		        Submit
		      </Button>

		  }


		</Form>


	)
}